using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow : MonoBehaviour
{
    public Transform target;
    public float speed = 4f;
    private Rigidbody rig;
    void Start()
    {
        rig = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

    }

    private void OnTriggerStay(Collider col)
    {
        if (col.tag == "Target")
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, target.position, speed * Time.fixedDeltaTime);
            rig.MovePosition(pos);
            transform.LookAt(target);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        
    }
}
