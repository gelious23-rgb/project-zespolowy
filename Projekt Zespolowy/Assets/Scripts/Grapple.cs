using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Grapple : MonoBehaviour
{
    [SerializeField] private float pullSpeed = 0.5f;
    [SerializeField] private float stopDistance = 4f;
    [SerializeField] private GameObject hookPrefab;
    [SerializeField] private Transform shootTransform;

    private Hook hook;
    private bool pulling;
    private Rigidbody rigid;
    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        pulling = false;
        

    }

    // Update is called once per frame
    void Update()
    {
        if (hook == null && Input.GetKeyDown(KeyCode.E))
        {
            StopAllCoroutines();
            pulling = false;
            hook = Instantiate(hookPrefab, shootTransform.position, Quaternion.identity).GetComponent<Hook>();
            hook.Initialized(this, shootTransform);
            StartCoroutine(DestroyHookAfterLifeTime());
        }
        
        else if (hook != null && Input.GetKeyDown(KeyCode.Q))
        {
            DestroyHook();
        }

        if (!pulling || hook == null) return;
        if (Vector3.Distance(transform.position, hook.transform.position) <= stopDistance)
        {
            DestroyHook();
        }
        else
        {
            rigid.AddForce((hook.transform.position - transform.position).normalized * pullSpeed, ForceMode.VelocityChange) ;
        }
    }

    public void StartPull()
    {
        pulling = true;
    }

    private void DestroyHook()
    {
        if (hook == null) return;

        pulling = false;
        Destroy(hook.gameObject);
        hook = null;
    }

    private IEnumerator DestroyHookAfterLifeTime()
    {
        yield return new WaitForSeconds(8f);
        DestroyHook();
    }
}
