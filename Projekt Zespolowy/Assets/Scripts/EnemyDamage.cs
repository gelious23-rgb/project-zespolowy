using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{

    public float timeStart;
    public int timeEnd;
    void Start()
    {
        
    }
    void Update()
    {
        timeStart += 1;
        if (timeStart >= timeEnd)
        {
            timeStart = 0;
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Enemy")
        {
            col.GetComponent<AISecurityBot1>().healthEnemy -= 5;
        }
    }
}
