using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;


public class AISecurityBot1 : MonoBehaviour
{
    private float timer;

    public GameObject Player;
    public GameObject panel;
    public Transform[] points;
    private int current;
    public float speed;

    public int startHealthEnemy;
    public int healthEnemy;

    public GameObject DropLootPrefab;
    private GameObject _droopLootTarget;
    
    public GameObject Gun;
    private void Start()
    {
        _droopLootTarget = GameObject.FindGameObjectWithTag("DroopLootTracker");
    }

    private void Update()
    {
        if (Player.GetComponent<Player>().currentHealth == 0)
        {
            panel.SetActive(true);
            Destroy(Player.gameObject);
            if (gameObject != null)
            {    
                // Do something  
                Destroy(gameObject);
            }
        }
        
        Death();
        Patroller();
    }



    void Death()
    {
        if (healthEnemy <= 0)
        {
            Destroy(gameObject);
            Player.GetComponent<Player>().currentSoft += 5000 ;
            Player.GetComponent<Player>().softbar.SetSoft(Player.GetComponent<Player>().currentSoft);
            Player.GetComponent<Player>().currentHealth += 15 ;
            Player.GetComponent<Player>().healthBar.SetHealth(Player.GetComponent<Player>().currentHealth);
            for (int i = 0; i < startHealthEnemy/50; i++)
            {
                var go = Instantiate(DropLootPrefab, transform.position + new Vector3(0, -1), Quaternion.identity);
                go.GetComponent<Follow>().Target = _droopLootTarget.transform;
            }
            WeaponDrop();
        }

    }

    void WeaponDrop()
    {
        Instantiate(Gun, transform.position, Quaternion.identity);
    }

    void Patroller()
    {
        if (transform.position != points[current].position)
        {

            transform.position = Vector3.MoveTowards(transform.position, points[current].position, speed * Time.deltaTime);
        }
        else
        {
            transform.Rotate(0, 180,0);
            current = (current + 1) % points.Length;
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Player")
        {
            Player = col.gameObject;
            gameObject.GetComponent<Animator>().SetBool("Fire", true);
            onFire();
            speed = 0;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            gameObject.GetComponent<Animator>().SetBool("Fire", false);
            speed = 4;
        }
    }

    void onFire()
    {
        timer += 1 * Time.deltaTime;
        if (timer >= 1.2f)
        {
            Player.GetComponent<Player>().currentHealth -= 20;
            Player.GetComponent<Player>().healthBar.SetHealth(Player.GetComponent<Player>().currentHealth);
            timer = 0;
        }
        
    }
}
