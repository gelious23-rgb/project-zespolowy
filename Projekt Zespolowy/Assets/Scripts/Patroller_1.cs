using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patroller_1 : MonoBehaviour
{
    public Transform[] points;
    private int current;
    public float speed;
    void Start()
    {
        current = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position != points[current].position)
        {

            transform.position = Vector3.MoveTowards(transform.position, points[current].position, speed * Time.deltaTime);
        }
        else
        {
            transform.Rotate(0, 180,0);
            current = (current + 1) % points.Length;
        }

    }
}
