using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoftBar : MonoBehaviour
{
    public Slider slider;
    public Image fill;
    public void SetMaxSoft(float soft)
    {
        slider.maxValue = soft;
        slider.value = soft;
    }

    public void SetSoft(float soft)
    {
        slider.value = soft;
    }
}
