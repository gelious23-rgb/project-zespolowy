using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int damageHealth;
    public GameObject player;
    public int maxHealth = 100;
    public int currentHealth;
    public int maxSoft = 100;
    public int currentSoft;
    public int softDamage;

    public HealthBar healthBar;
    public SoftBar softbar;

    public GameObject panel;
    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        currentHealth = maxHealth;
        softbar.SetMaxSoft(maxSoft);
        currentSoft = maxSoft;
    }

    // Update is called once per frame
    void Update()
    {
        currentSoft = currentSoft - softDamage;
        softbar.SetSoft(currentSoft);
        
        if (currentSoft == 7000)
        {
            TakeDamage(20);
        }
        else if (currentSoft == 5000)
        {
            damageHealth = currentHealth * 25 / 100;
            TakeDamage(damageHealth);
        }
        else if (currentSoft == 2500)
        {
                damageHealth = currentHealth * 25 / 100;
                TakeDamage(damageHealth);
        }
        else if (currentSoft <= 0)
        {
                Restart();
        }
        

        if (currentHealth == 0)
        {
            Restart();
        }
    }

    void Restart()
    {
        panel.SetActive(true);
        Destroy(player.gameObject);
    }
    void TakeDamage(int damage)
    {
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
    }
}
