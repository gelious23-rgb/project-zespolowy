using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EndGame : MonoBehaviour
{
    public GameObject panel;
    public GameObject Player;
    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            panel.SetActive(true);
            Destroy(Player.gameObject);
        }
    }
}
