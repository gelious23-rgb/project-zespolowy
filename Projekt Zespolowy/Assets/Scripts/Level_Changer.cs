using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level_Changer : MonoBehaviour
{
    public int sceneindex;

    private void OnTriggerEnter(Collider myCollider)
    {
        if(myCollider.tag == ("Player"))
        {
            SceneManager.LoadScene(sceneindex);

        }
    }
}
